#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void mystrcpy(char s[], char t[]) {
	int length, i;

	length = strlen(s);
	for(i = 0; i < length; i++) {
		t[i] = s[i];
	}
	t[length] = '\0';
}

void mystrcpyBypointer(char *s, char *t) {
	int length, i;

	length = strlen(s);
	for(i = 0; i < length; i++) {
		*(t + i) = *(s + i);
	}
	t[length] = '\0';
}

float sumNonRecursive(int N, float values[]){
	int i;
	float result = 0;

	
	for(i = 0; i < N; i++) {
		result = result + values[i];
	}

	return result;
}

float sumRecursive(int N, float values[]){
	float result = 0;
	int n = N-1;
	if(N > 0 || N != 0) {
		result = sumRecursive(N-1, values) + values[n];
	}
	return result;
}


void main(int argc, char* argv[]) {
	char bufs[BUFSIZ];
	char buft[BUFSIZ];
	float sumNR, sumR;
	int N = 5;
	int i;
	float values[5] = {1.0, 2.0, 3.0, 4.0, 5.0};

	if (argc != 5) {
		fputs("에러! 옵션을 제대로 입력하지 않으셨군요...\n", stderr);
		exit(EXIT_FAILURE);
	}

	/*과제1,2 문자열 입력 받기*/
	printf("복사할 문자열을 입력하세요\n");
	scanf("%s", bufs);

	/*과제1 배열을 이용하여 입력받은 문자열 복사하기*/
	mystrcpy(bufs, buft);
	printf("\nAssignment1-1 : array\n복사 전 : %s\n복사 후 : %s\n", bufs, buft);

	/*과제2 포인터을 이용하여 입력받은 문자열 복사하기*/
	mystrcpyBypointer(bufs, buft);
	printf("\nAssignment1-2 : pointer\n복사 전 : %s\n복사 후 : %s\n", bufs, buft);

	/*과제3 명령 인자로 받은 문자들을 앞부분만 출력하기*/
	printf("\nAssignment1-3 : input = test a1 b2 c3\n");
	printf("%s ", argv[1]);
	for(i = 2; i < argc; i++) {
		printf("%c ", argv[i][0]);
	}
    printf("\n");

	/*과제4 배열의 모든 값들의 합을 recursive함수와 nonrecursive함수로 구하기*/
	sumNR = sumNonRecursive(N, values);
	sumR = sumRecursive(N, values);
	printf("\nAssignment1-4 : NonRecursive\n 1 + 2 + 3 + 4 + 5 = %f\n", sumNR);
	printf("Assignment1-4 : Recursive\n 1 + 2 + 3 + 4 + 5 = %f\n", sumR);
	
}